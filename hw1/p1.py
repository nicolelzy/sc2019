"""M345SC Homework 1, part 1
Nicole Zhi Ying LEE (01196496)
"""

def ksearch(S,k,f,x):
    """
    Search for frequently-occurring k-mers within a DNA sequence
    and find the number of point-x mutations for each frequently
    occurring k-mer.
    Input:
    S: A string consisting of A,C,G, and Ts
    k: The size of k-mer to search for (an integer)
    f: frequency parameter -- the search should identify k-mers which
    occur at least f times in S
    x: the location within each frequently-occurring k-mer where
    point-x mutations will differ.

    Output:
    L1: list containing the strings corresponding to the frequently-occurring k-mers
    L2: list containing the locations of the frequent k-mers
    L3: list containing the number of point-x mutations for each k-mer in L1.

    Discussion: a) I conducted the search for the frequently occurring k-mers by storing the counts and indices of the occurences of each kmers in 2 dictionaries, freq (counts) and D (indices). For each k-mer formed, if the k-mer is in freq, the count is updated in freq and the index of the first character is stored in D. Each key has a sub-list of values in D. If the k-mer is not found, a new key and value is stored. By using dictionaries, it becomes efficient to pick the k-mers which occur more than f times, and at the same time, we can search for the location of the relevant frequent k-mers. To find the number of point-x mutations for each k-mer in L1, we create a zero vector, the size of L1, list of all k-mers and a list without the nucleotide in its x-th position. We also have a list of all L1's kmers without its x-th nucleotide. By comparing the two lists and using the dictionary, we can sum all the point-x mutations without adding the identical k-mer to the count of point-x mutations.

    b) The asymptotic run time for creating 2 dictionaries, assuming constant time taken for key lookup and insertion or addition, is O(len(S) - k + 1), which is asymptotic to the number of non-unique k-mers formed from S. Since we only do the key lookup once for 2 dictionaries, its runtime is still O(len(S) - k + 1). For a key lookup, and accessing the corresponding value takes constant time. If we let there be N unique k-mers and M frequently occurring k-mers, we will have a total run time of O(MHence we require O(N+M). Similarly, L3 compares the M k-mers to N unique k-mers and for each mutation found, it involves one key lookup. So if there are J unique k-mers which are point-x mutations, the worst case total run-time is O(MN+J). Depending on the input, the values of J, M and N will change accordingly and affect the run-time.
    """

    # Initializes empty dictionaries
    freq = {}
    D = {}
    # Initialize L1 and L2
    L1 = []
    L2 = []
    for i in range(len(S)-k+1):
        kmer = S[i:i+k]

        if kmer in freq:
            freq[kmer] +=1
            D[kmer] += [i]
        else:
            freq[kmer] = 1
            D[kmer] = [i]

    # Filling L1 and L2 simultaneously
    for keys, values in freq.items():
        if values > f:
            L1.append(keys)
            L2.append(D[keys])

    # Determining point-x mutations
    L3 = [0 for k in L1]
    kmers = list(freq.keys())
    unique_kmers = [seq[:x]+seq[x+1:] for seq in kmers]

    L1_x = [seq[:x]+seq[x+1:] for seq in L1]

    for i in range(len(L1_x)):
        for j in range(len(unique_kmers)):
            if (L1_x[i] == unique_kmers[j]) & (kmers[j]!=L1[i]):
                L3[i] += freq[kmers[j]]

    return L1, L2, L3


if __name__=='__main__':
    #Sample input and function call. Use/modify if/as needed
    S='CCCTATGTTTGTGTGCACGGACACGTGCTAAAGCCAAATATTTTGCTAGT'
    #infile = open('test_sequence.txt','r')
    #S = infile.read() #(long) gene sequence
    #infile.close()
    k, x, f = 3, 2, 2
    L1,L2,L3 = ksearch(S,k,f,x)
