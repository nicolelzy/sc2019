"""M345SC Homework 1, part 2
Nicole Zhi Ying LEE (01196496)
"""

def nsearch(L,P,target):
    """Input:
    L: list containing *N* sub-lists of length M. Each sub-list
    contains M numbers (floats or ints), and the first P elements
    of each sub-list can be assumed to have been sorted in
    ascending order (assume that P<M). L[i][:p] contains the sorted elements in
    the i+1th sub-list of L
    P: The first P elements in each sub-list of L are assumed
    to be sorted in ascending order
    target: the number to be searched for in L

    Output:
    Lout: A list consisting of Q 2-element sub-lists where Q is the number of
    times target occurs in L. Each sub-list should contain 1) the index of
    the sublist of L where target was found and 2) the index within the sublist
    where the target was found. So, Lout = [[0,5],[0,6],[1,3]] indicates
    that the target can be found at L[0][5],L[0][6],L[1][3]. If target
    is not found in L, simply return an empty list (as in the code below)
    """

    N = len(L)
    M = len(L[0])
    Lout = []

    for i in range(N):
        inds = []
        if target <= L[i][P-1]:
            istart = 0
            iend = P - 1

            # Loop to find first occurence of target in sorted list
            while istart <= iend:
                imid = int(0.5*(istart+iend))
                if target <= L[i][imid]: iend = imid - 1
                elif target > L[i][imid]: istart = imid + 1

                # if while loop terminates index will automatically update to the smallest index
                if ((imid == 0 or target > L[i][imid - 1]) and L[i][imid] == target) :
                    first = imid
                    if target == L[i][imid + 1]:
                        istart = first
                        iend = P - 1

                        # Loop to find last occurence of target in sorted list
                        while istart <= iend:
                            imid = int(0.5*(istart+iend))
                            if target < L[i][imid]: iend = imid - 1
                            elif target >= L[i][imid]: istart = imid + 1

                            # if while loop terminates index will automatically update to the largest index
                            if (imid == P - 1 or target < L[i][imid + 1]) and target == L[i][imid]:
                                inds = list(range(first, imid+1))

        # Append index of occurences in remaining M-P
        for j in range(M - P):
            if target == L[i][P+j]:
                inds.append(P+j)

        # If for inds non-empty for sublist i, append to Lout
        if inds != []:
            Lout.append([i, inds])
    return Lout


def nsearch_time(Pin = 200, Min=300, Nin = 1000, display = False):
    """Analyze the running time of nsearch.
    Add input/output as needed, add a call to this function below to generate
    the figures you are submitting with your codes.

    Discussion: (Note log = log of base 2)
    a) My algorithm searches through each of the N sub-lists of length M for the target, using binary search within the sorted list, L[i][:P], and linear search for the unsorted list, L[i][P:]. Since the binary search stops the loop once a target is found or while condition is not satisfied, only one index is found for each binary search. However, to find all repeated occurences of the target, the smallest index, where the target is located, is first determined. If it exists, we find the largest index where the target is located and append all the integers between the smallest and largest index to inds, the array where the indexes of the target location is stored. For all non-empty inds, we append the list index, i, and inds, as a 2-element list to Lout.

    b) A binary search occurs log(P) times to find the first occurrence (we call it X). Then, if the following number is equal to the target, we do another binary search, with the cost log(P-X+1). Now, we do a linear comparison M-P times to find all reptitions of the target. So a total run-time would be at most 2*log(P) + M - P. This would depend on the input of M and P. Since we search through all N sublists, we multiply it by N for the total run time.

    c) Since my algorithm is segmented, I will explain why each part is efficient. Running a binary search on the sorted list is one of the most efficient ways to find the smallest and largest indices for the target locations. As in (b), we have derived that a binary search has the asymptotic run time of O(log(P)). I opted for a linear search on the remaining unsorted list because sorting the list then running 2 binary searches would be less efficient than looking through every item in the array, which would have at most an asymptotic run time of O(M-P). In the worst-case scenario, a mergesort will have a cost of O((M-P)*log(M-P) + 2*log(M-P)), while a linear search has O(M-P). The latter is smaller for large N and hence I opted to do a linear search for the M-P numbers.

    d) (See output)

    e) From the fig1, we see that as we change the proportion of P, keeping M constant, the walltime for the search decreases as the value of P increases. This is due to the fact that a binary search is much faster than a linear search. By increasing the value of P, we are also looking at the trend of decreasing length (M-P) or rather the length of the unsorted list. This is expected as we know from lectures that binary search is a much faster and hence more efficient search.

    From fig2, we see that if the fraction of P/M is constant at 0.6, as M increases, or rather the length of each list increases, the run time increases as well. This is expected for larger M.

    As illustrated in fig 3, as N increases, with P and M kept constant, we see that the run time increases there is no efficient way to search all N sub-lists at once, but rather we must repeat the searches N times.
    """
    import numpy as np
    import matplotlib.pyplot as plt
    import time

    Pval = np.linspace(20, 280, 20, dtype=np.int32)
    Ptime = np.zeros((len(Pval), 100))

    # Keeping M and N constant, we change P, to see if changing the proportion of sorted and unsorted elements of each sub-lists affects run-time

    for i, P in enumerate(Pval):
        for j in range(100):
            L = [np.concatenate((sorted(np.random.randint(0, 100, P)),np.random.randint(0, 100, Min-P))) for n in range(Nin)]

            t1 = time.time()
            _ = nsearch(L, P, 97)
            t2 = time.time()
            Ptime[i, j]= t2-t1


    # Keeping N constant, we change M and hence P, to see if changing the changing the length of the sublist (with P/M constant) affects run-time

    Mval = np.linspace(100, 500, 21, dtype=int)
    Mtime = np.zeros((len(Mval),100))
    for i, M in enumerate(Mval):
        for j in range(100):
            P = int(0.6*M)
            L = [np.concatenate((sorted(np.random.randint(0, 100, P)),np.random.randint(0, 100, M-P))) for n in range(Nin)]

            t1 = time.time()
            _ = nsearch(L, P, 97)
            t2 = time.time()
            Mtime[i, j] = t2-t1

    # Keeping M and P constant, we change N to see the change in run-time
    Nval = np.linspace(100, 1000, 21, dtype=int)
    Ntime = np.zeros((len(Nval),100))
    for i, N in enumerate(Nval):
        for j in range(100):
            L = [np.concatenate((sorted(np.random.randint(0, 100, Pin)),np.random.randint(0, 100, Min-Pin))) for n in range(N)]

            t1 = time.time()
            _ = nsearch(L, Pin, 97)
            t2 = time.time()
            Ntime[i,j] = t2-t1

    Ptime = mean(Ptime, axis=1)
    Mtime = mean(Mtime, axis=1)
    Ntime = mean(Ntime, axis=1)

    if display:
        # Walltime Plot
        plt.figure()
        plt.semilogx(Pval, Ptime, 'x--')
        plt.xlabel('P')
        plt.ylabel('Walltime (s)')
        plt.title('Walltime for changing P value will keeping M and N constant')

        plt.figure()
        plt.semilogx(Mval, Mtime, '^--')
        plt.xlabel('M')
        plt.ylabel('Walltime (s)')
        plt.title('Walltime for changing M value will keeping N constant (P/M is constant)')

        plt.figure()
        plt.semilogx(Nval, Ntime, 'o--')
        plt.xlabel('N')
        plt.ylabel('Walltime (s)')
        plt.title('Walltime for changing number of sub-lists, N, will keeping P and M constant.')

    return None


if __name__ == '__main__':

    #add call(s) to nsearch here which generate the figure(s)
    #you are submitting
    output = nsearch_time(display=True) #modify as needed
