"""M345SC Homework 2, part 1
Nicole Zhi Ying LEE (01196496)
"""

def scheduler(L):
    """
    Question 1.1
    Schedule tasks using dependency list provided as input

    Input:
    L: Dependency list for tasks. L contains N sub-lists, and L[i] is a sub-list
    containing integers (the sub-list may also be empty). An integer, j, in this
    sub-list indicates that task j must be completed before task i can be started.

    Output:
    S: A list of integers corresponding to the schedule of tasks. S[i] indicates
    the day on which task i should be carried out. Days are numbered starting
    from 0.

    Discussion:
    Here, I assumed that M = 0 and all tasks are completed on Day 0, while leaving the queue empty. I then search through L to find the (N-M) empty sublists, marking them as completed and adding the remaining M tasks to the queue. This has an asymptotic runtime of O(N). Within the queue, I iterate through the queue once each day. Using the list C (of completed tasks), I check if all tasks within each sublist is completed. If task i can be carried out, I remove i from the queue and update C[i] and S[i]. To ensure that we look at it on a day to day basis, C1 is the copy of C from the preivous day, and checks are made against C1 and not C. For the worst case scenario, we will have an asymptotic run-time of O(N+M!) and in the best case scenario, we will have the runtime of O(N+M).

    This is efficient as it mainly searches through the uncompleted tasks and keeps all tasks in the queue until it is completed. Since appending and popping have an asymptotic runtime of O(1) each, by only removing the task from the queue when completed, we ensure minimum runtime.
    """

    # N - M empty sublists are dealt with directly
    C = [0 for l in L] # Labels for Completed(1)/Uncompleted(0) Tasks
    S = [0 for l in L] # S the output
    Q = [] # Queue for M tasks
    day = 1 # day counter for subsequent tasks

    # Mark N - M empty tasks as completed in C and append uncompleted tasks to Q
    for i, SL in enumerate(L):
        if len(SL) == 0:
            C[i] = 1
        else:
            Q.append(i)

    # Iterating through Queue, Q
    while len(Q) > 0:
        C1 = C.copy() # Copy of C (C from day - 1)
        # Compare to list of completed tasks from previous day, check if all
        # tasks in sublists of the M tasks are completed. If all tasks in L[i]
        # are completed, then task i is marked as completed and removed from
        # the queue. The day of completion is added to S, the output.
        for i in Q:
            #sum of tasks using C1 == len(L[i]) if all tasks in L[i] are
            # complete
            if sum([C1[x] for x in L[i]]) == len(L[i]):
                C[i] = 1
                S[i] = day
                Q.remove(i)
        day += 1 # day counter increases at end of the day

    return S


def findPath(A,a0,amin,J1,J2):
    """
    Question 1.2 i)
    Search for feasible path for successful propagation of signal
    from node J1 to J2

    Input:
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the sub-list my also be empty) of the form (j,Lij). The integer,
    j, indicates that there is a link between nodes i and j and Lij is the
    loss parameter for the link.

    a0: Initial amplitude of signal at node J1

    amin: If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine if the signal can successfully reach node J2 from node J1

    Output:
    L: A list of integers corresponding to a feasible path from J1 to J2.

    Discussion:
    By noting that if the signal reaches a junction successfully, it is boosted back to a0. Thus, we can determine which links in the adjacency matrix A will allow the signal to be successfully propogated by checking if a0*L_ij surpasses the threshold amin. For all links where a0*L_ij >= amin, the links are stored in another adjacency list, A1. (All edges in A1 will allow a0 to propagate between nodes i and j successfully). Suppose that the graph has E edges, then the adjacency matrix A has 2E tuples. By checking each loss parameter (noting that L_ij = L_ji), the asymptotic runtime of creating A1 is given by O(E). Then, DFS is used to find the possible path between J1 and J2 using the new adjacency list A1. If there are N nodes (or junctions), and A1 has 2M tuples (E>=M), then the asymptotic runtime of the DFS is given as O(N+M).

    Now, the total asymptotic runtime would be O(N+M+E), which is linear. For large N, M and E, the asymptotic run time would not increase exponentially and hence is efficient.
    """
    # copy of A with edges that satisfy a0*Lij >= amin
    A1 = [[] for i in range(len(A))]

    # Determining all edges where condition is satisfied (signal does not terminate)
    for i in range(len(A)):
        for pair in A[i]:
            if (a0*pair[1] >= amin) & (pair not in A1[i]):
                # Symmetric entries are entered simultaneously for efficiency
                A1[i].append(pair)
                A1[pair[0]].append((i, pair[1]))

    Q = [J1] # Queue for unexplored junctions
    path = [[J1]] #Path
    L = [] # Final output, updated if path found, otherwise empty

    # Finding all possible paths from J1 to J2
    while len(Q) > 0:
        # Remove last node from queue (similarly for path)
        junction = Q.pop()
        path_i = path.pop()

        # For all neighbours of the junction
        for n in A1[junction]:
            # If the neighbours are not in the path, we append them to the paths
            if n[0] not in path_i:
                # If the final node is reached (J2), we update L and end
                if n[0] == J2:
                    L = path_i+[n[0]]
                    break
                else:
                    # add unexplored neighbors to back of queue
                    Q.append(n[0])
                    # Add incomplete path back to the queue
                    path.append(path_i+[n[0]])

    return L


def a0min(A,amin,J1,J2):
    """
    Question 1.2 ii)
    Find minimum initial amplitude needed for signal to be able to
    successfully propagate from node J1 to J2 in network (defined by adjacency list, A)

    Input:`
    A: Adjacency list for graph. A[i] is a sub-list containing two-element tuples (the
    sub-list my also be empty) of the form (j,Lij). The integer, j, indicates that there is a link
    between nodes i and j and Lij is the loss parameter for the link.

    amin: Threshold for signal boost
    If a>=amin when the signal reaches a junction, it is boosted to a0.
    Otherwise, the signal is discarded and has not successfully
    reached the junction.

    J1: Signal starts at node J1 with amplitude, a0
    J2: Function should determine min(a0) needed so the signal can successfully
    reach node J2 from node J1

    Output:
    (a0min,L) a two element tuple containing:
    a0min: minimum initial amplitude needed for signal to successfully reach J2 from J1
    L: A list of integers corresponding to a feasible path from J1 to J2 with
    a0=a0min
    If no feasible path exists for any a0, return output as shown below.

    Discussion:
    Similar to findPath, I use DFS to find all possible paths from J1 to J2 and determine the highest minimum loss parameter of all possible paths. Within each path found, amin/min(loss of each node in the path) gives the smallest a0 for the path. Let the largest loss parameter in the path be A and the smallest loss parameter in the path be B (B < A). Then amin/A < amin/B. However, amin/A*B = amin * B/A < amin, so the minimum initial amplitude a0 of each path to successfully reach J2 from J1 must be amin/B. We aim to find the largest possible "B" from all paths, which is equivalent to finding the highest minimum loss parameter of each path out of all paths.

    DFS has an asymptotic runtime of O(N+M) operations, where N is the number of nodes between J1 and J2, and M is the total number of edges between J1 and J2.

    Once all paths are found, if we let there be P paths found, then finding the minimum loss for each path has an asymptotic run time of O(P) and finding the path with the highest minimum loss parameter also has O(P) operations. Therefore, the total asymptotic run time of all operations in a0min is O(N+M+2P), which is linear and hence efficient.
    """

    Q = [J1] # Queue
    path = [[J1]] #Path
    loss = [[1]] # List of loss parameters in path

    # Finding all possible paths from J1 to J2
    while len(Q) > 0:
        # Removing last element of the Queue, and the path and loss queue
        junction = Q.pop()
        path_i = path.pop()
        loss_i = loss.pop()

        # Iterating through the neighbours of each junction, we find all possible paths and combinations of loss parameters
        for n in A[junction]:
            if n[0] not in path_i:
                if n[0] == J2:
                    # When path is complete, it is added to the front of the
                    # queue, so new elements are not added to the completed path
                    path = [path_i+[n[0]]] + path
                    loss = [loss_i+[n[1]]] + loss
                    break
                else:
                    # add unexplored neighbors to back of queue
                    Q.append(n[0])
                    # Add incomplete paths and combinations of loss parameters
                    # to the back of the queue
                    path.append(path_i+[n[0]])
                    loss.append(loss_i+[n[1]])

    # Finding highest minimum loss of all paths
    # If a path is found, we select the smallest loss parameter within each path (in loss)
    if path != []:
        loss = [min(l) for l in loss]
        # Pick the path with the highest minimum loss, L
        for i, l in enumerate(loss):
            if l == max(loss):
                path_out = path[i]
        # Identifying output as required
        output = amin/max(loss), path_out
    else:
        # If no possible paths found
        output = -1, []

    return output

if __name__=='__main__':
    #add code here if/as desired
    # Test for Part1.1
    L = [[], [0], [], [4, 5, 13], [14], [], [0, 30], [1, 12], [2, 7], [6, 10,
        20], [8], [33], [32], [], [26, 32], [], [4], [15, 22, 30], [15], [11,
        29], [13], [19], [4, 7], [1, 31], [22], [0, 22], [21], [], [], [], [],
        [3, 6], [2, 11], []]
    S = scheduler(L)

    # Test for Part 1.2
    C = [[(1, 0.6), (2, 0.4), (6, 0.7), (7, 0.5)], [(0, 0.6), (7, 0.8)], [(0,
        0.4), (6, 0.7)], [(4, 0.6), (7, 0.3)], [(3, 0.6), (7, 0.9)], [], [(0,
        0.7), (2, 0.7)], [(0, 0.5), (1, 0.8), (3, 0.3), (4, 0.9)]]
    L1 = findPath(C, 1.2, 0.72, 6, 7)
    output = a0min(C, 0.84, 6, 7)
