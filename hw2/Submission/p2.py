"""M345SC Homework 2, part 2
Nicole Zhi Ying LEE (01196496)
"""
import numpy as np
import networkx as nx
from scipy.integrate import odeint
import matplotlib.pyplot as plt


def model1(G,x=0,params=(50,80,105,71,1,0),tf=6,Nt=400,display=False):
    """
    Question 2.1
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    S: Array containing S(t) for infected node
    """
    a, theta0, theta1, g, k, tau = params
    tarray = np.linspace(0,tf,Nt+1)
    S = np.zeros(Nt+1)

    #Add code here
    # Precompute theta
    theta = theta0 + theta1*(1-np.sin(2*np.pi*tarray))
    # dt = tf/Nt = tarray[1]
    # arrays for V and I of affected node
    V = np.zeros(Nt+1)
    I = np.zeros(Nt+1)
    V[0], I[0], S[0] = 0.1, 0.05, 0.05 # Initial condition

    # Time marching
    for i in range(Nt):
        V[i+1] = V[i] + tarray[1]*(k - (k+theta[i]*S[i])*V[i])
        I[i+1] = I[i] + tarray[1]*(theta[i]*S[i]*V[i] - (a+k)*I[i])
        S[i+1] = S[i] + tarray[1]*(a*I[i]-(g+k)*S[i])


    if display:
        # Plot of S(t) for node x against time
        plt.figure()
        plt.plot(tarray, S)
        plt.title("Plot of S(t) for the infected node x = %d against time" %x)
        plt.xlabel("Time(t)")
        plt.ylabel("S(t)")

    return S

def modelN(G,x=0,params=(50,80,105,71,1,0.01),tf=6,Nt=400,display=False):
    """
    Question 2.2
    Simulate model with tau=0

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    tf,Nt: Solutions Nt time steps from t=0 to t=tf (see code below)
    display: A plot of S(t) for the infected node is generated when true

    x: node which is initially infected

    Output:
    Smean,Svar: Array containing mean and variance of S across network nodes at
                each time step.
    """
    a,theta0,theta1,g,k,tau=params
    tarray = np.linspace(0,tf,Nt+1)
    Smean = np.zeros(Nt+1)
    Svar = np.zeros(Nt+1)

    #Add code here
    A = nx.adjacency_matrix(G).toarray() #Adjacency Matrix
    q = [d[1] for d in G.degree()] #q[i] = q_i
    # Flux Matrix by columns
    F = np.zeros(A.shape)
    for j in range(len(A)):
        # Each column has the same denominator (np.dot(q, A[:,k]))
        # Taking maximum of 10**-6 or dot product ensures that denominator not
        # 0 (prevents 0/0)
        denom = max(10**-6, np.dot(q, A[:, j]))
        F[:, j] = q*A[:,j]/denom
    F = tau*F #tau is multiplied to all F_ij, so elementwise multiplication is more efficient

    def RHS(y,t):
        """Compute RHS of model at time t
        input: y should be a 3N x 1 array containing with
        y[:N],y[N:2*N],y[2*N:3*N] corresponding to
        S on nodes 0 to N-1, I on nodes 0 to N-1, and
        V on nodes 0 to N-1, respectively.
        output: dy: also a 3N x 1 array corresponding to dy/dt

        Discussion:
        For each dSi/dt, sum(F[i,j]*S[j] - F[j,i]*S[i]) over j requires O(3N) operations (N as defined). Then a*I[i] - (g+k)*S[i] requires O(1) operation for each addtion, subtraction or multiplication. For each dSi/dt, O(3N+4) operations are used.

        Then, for one call to RHS, dS/dt is computed using dot products and S, I, V are only sliced from y once, so this is O(3N) operations required. Eachdot product has O(N(N+(N-1))) = O(N(2N-1)), and S*np.sum(F) has O(N+N*N) operations. So in total, O(3N+5N+N(2N-1)+N(N+1)) = O(11N**2), which on overage over the N dSi/dt gives O(11*N) operations each. This is assuming that every elementwise addition, subtraction and multiplication is taken as 1 operation each.

        Use of dot products and elementwise vector-matrix multiplication can allow calculations to be done at once, making the construction simple and efficient.
        """
        # Identifying S, I, V
        N = int(y.size/3)
        S, I, V = y[:N], y[N:2*N], y[2*N:3*N]
        theta = theta0 + theta1*(1 - np.sin(2*np.pi*t))

        # dy/dt = [dS/dt dI/dt dV/dt], dS/dt, dI/dt and dV/dt respectively
        dy = np.zeros(3*N)
        dy[:N] = a*I - (g+k)*S + np.dot(F, S) - S*np.sum(F, axis=0)
        dy[N:2*N] = theta*S*V -(a+k)*I + np.dot(F, I) - I*np.sum(F, axis=0)
        dy[2*N:3*N] = k*(1-V) - theta*S*V + np.dot(F,V) - V*np.sum(F, axis=0)
        return dy

    # Initial conditions, setting y0 = 3N*1 array
    N = len(G.nodes())
    y0 = np.zeros(2*N) # S, I = 0
    y0 =np.append(y0, np.ones(N)) # V = 1
    y0[x], y0[N+x], y0[2*N+x] = 0.05, 0.05, 0.1 # S[x], I[x], V[x]

    # Optimising model for all nodes at the Nt timesteps
    y = odeint(RHS,y0,tarray)

    # Computing mean and variance across N at all Nt timesteps
    Smean = np.mean(y[:,:N], axis=1)
    Svar = np.var(y[:,:N], axis=1)

    if display:
        # Plot of mean
        plt.figure()
        plt.plot(tarray, Smean)
        plt.title("Plot of average of S(t) across N nodes against time")
        plt.xlabel("Time(t)")
        plt.ylabel("$<S(t)>$")
        # Plot of variance
        plt.figure()
        plt.plot(tarray, Svar)
        plt.title("Plot of average of variance of S(t) across N nodes against time")
        plt.xlabel("Time(t)")
        plt.ylabel("$<(S(t) - <S(t)>)^2>$")

    return Smean, Svar



def diffusion(input=(0, 100, 5, 60, 400), display=False):
    """Analyze similarities and differences between simplified infection model
    and linear diffusion on Barabasi-Albert networks.
    Modify input and output as needed.

    Discussion:
    Given a, theta1, g and k are all taken as 0 for the simplified infection
    model, dS/dt only depends on the Flux matrix, F, and the array of state of
    all spreaders in the network. This looks similar to the linear diffusion
    equation where dS/dt = -D*sum(L[i,j]*S[j]) (sum over j). As such, we would
    expect the simplified infection model to have similar graph as the linear
    diffusion, for Svar found in modelN. We see that the gradient and curve of
    the graph changes as tau changes.

    Difference -
    F is a product of q and A, so the main diagonal of F is always 0, while L
    is the difference between Q (diagonal matrix with q on its main diagonal)
    and A, so its main diagonal is never 0, unless the node is isolated.
    Furthermore, F represents the probability that the agent will spread to
    the adjacent nodes per unit time, while L simply represents the
    connectivity of each node in the networkself.

    The plot shows that regardless of the change in tau, the mean value of the
    simplified model remains the same at 0.05. As tau increases in value,
    between 0 to 1, the variance changes from constant variance to decreasing
    decrease over time. On average, the spread is contained for the simplified model so the mean is constant.

    For the linear diffusion, the solutions are linear combinations of the eigenvectors and eigenvalues of L. In the linear diffusion of Barabasi-Albert Networks, the non-negative eigenvalues in e imply that the solution approaches an equilibrium, because it either remains constant (as when tau is 0) or decays exponentially as seen in the figure2.
    """
    x, n, m, tf, Nt = input
    G = nx.barabasi_albert_graph(n, m)
    tarray = np.linspace(0, tf, Nt+1)
    SSmean = np.zeros((Nt+1, 21))
    SSvar = np.zeros((Nt+1, 21))
    tauarray = np.linspace(0, 1, 21)

    for i, tau in enumerate(tauarray):
        SSmean[:,i], SSvar[:,i] = modelN(G, params = (0, 80, 0, 0, 0, tau), tf = tf, Nt = Nt)

    #Linear Diffusion on Barabasi-Albert Networks
    L = nx.laplacian_matrix(G).toarray() #Adjacency Matrix
    e, v = np.linalg.eig(L)

    if display:
        plt.figure()
        for i in range(21):
            plt.plot(tarray, SSmean[:,i], label="tau=%f"%tauarray[i])
        plt.legend()
        plt.xlabel('Time (t)')
        plt.ylabel('Mean of Simplified Infected Model')
        plt.title('Plot of Mean of Simplified Infected Model against Time, with varied tau')
        plt.show()
        plt.figure()
        for i in range(21):
            plt.plot(tarray, SSvar[:,i], label="tau=%f"%tauarray[i])
        plt.legend()
        plt.xlabel('Time (t)')
        plt.ylabel('Variance of Simplified Infected Model')
        plt.title('Plot of Variance of Simplified Infected Model against Time, with varied tau')
        plt.show()

    return G, L, e, v


if __name__=='__main__':
    #add code here to call diffusion and generate figures equivalent
    #to those you are submitting
    G, L, e, v = diffusion(display=True) #modify as needed
