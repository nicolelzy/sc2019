"""M345SC Homework 3, part 2
Nicole Zhi Ying LEE (01196496)
"""
import numpy as np
import networkx as nx
import scipy.linalg as la
from scipy.sparse import diags

def growth1(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.1
    Find maximum possible growth, G=e(t=T)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion:
    Given the ODE, we can express dy/dt = M*y (y as defined in the output),
    where M is a matrix containing the coefficients of elements in y. Then we
    can write y(t) = exp(M*T)*y(0) for the system of linear ODEs. We now
    express y(t) as b, y(0) as x and A = exp(M*T), to give Ax=b. Since we want
    to find the maximum possible growth of perturbation energy, e(t=T)/e(t=0),
    this is also equivalent to bTb/xTx, which is the maximization problem of
    Ax=b. We want to maximise b with norm of x being 1 (also e(t=0) = |x| = 1),
    which as shown in lectures, bTb is less than or equal to the largest
    eigenvalue, so the maximum growth is given by the largest eigenvalue. Now,
    this can be computed by the singular value decomposition of matrix A, to
    give U, S and VT. S consists of all square roots of eigenvalues, so G =
    S[0]**2. Now, by properties of eigenvalues and eigenvectors, the
    corresponding eigenvector, VT[0,:] gives the initial condition of G.
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------
    #Add code here
    # Building matrix A for dydt = M*y
    zv = np.ones(N)
    colF = F.sum(axis=0)
    main = np.hstack([-(g+k)*zv-colF, -(k+a)*zv-colF, k*zv-colF])
    ad = np.hstack([a*zv, np.zeros(N)])
    td = np.hstack([theta*zv, np.zeros(N)])
    bd = -theta*zv
    M = np.diag(main) + np.diag(bd, -2*N) + np.diag(td, -N) + np.diag(ad, N) + la.block_diag(*([F] *3))

    #y(T) = exp(M*T)*y(0)
    A = la.expm(np.array(M)*T)
    # Solving for Maximum Growth Ax = b, maximizing bTb
    U, S, Vt = la.svd(A)

    #x = eigenvector of largest eigenvalue
    y = Vt[0,:]
    G = S[0]**2 #bTb < largest eigenvalue

    return G, y

def growth2(G,params=(0.02,6,1,0.1,0.1),T=6):
    """
    Question 2.2
    Find maximum possible growth, G=sum(Ii^2)/e(t=0) and corresponding initial
    condition.

    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth
    y: 3N-element array containing computed initial condition where N is the
    number of nodes in G and y[:N], y[N:2*N], y[2*N:] correspond to S,I,V

    Discussion:
    As in growth1, given the ODE, we can express dy/dt = M*y (y as defined in
    the output), so we have the system of linear ODES, y(t) = exp(M*T)*y(0). We
    keep A = exp(M*T), x = y(0) and b = y(t), so Ax = b. To maximize the growth
    for G=sum(Ii^2)/e(t=0), this is also equivalent to bTPb/xTx, which is the
    maximization problem of PAx=Pb, where P is the projection matrix. In order
    to isolate I, we want P to be a diagonal matrix, with 1's along the main
    diagonals in rows N to 2N-1. P is clearly symmetric and it is also
    idempotent, so when multiplied to RHS and LHS of Ax=b, we get PAx = b, and
    xTATPTPAx = xTATP^2Ax = xTATPAx = bTPTPb = bTP^2b = bTPb = sum(Ii^2) So
    now, we simply rewrite this as xTDTDx = b'Tb', where D = P*A and b' = P*b.
    Now maximizing the new problem statement will solve the problem as in
    growth1 (using SVD, taking largest eigenvalue and corresponding
    eigenvector) for growth2, where it is assumed that |x| = 1.
    """
    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------

    #Add code here
    # Building matrix A for dydt = M*y
    zv = np.ones(N)
    colF = F.sum(axis=0)
    main = np.hstack([-(g+k)*zv-colF, -(k+a)*zv-colF, k*zv-colF])
    ad = np.hstack([a*zv, np.zeros(N)])
    td = np.hstack([theta*zv, np.zeros(N)])
    bd = -theta*zv
    M = np.diag(main) + np.diag(bd, -2*N) + np.diag(td, -N) + np.diag(ad, N) + la.block_diag(*([F] *3))

    #y(T) = exp(M*T)*y(0)
    A = la.expm(np.array(M)*T)
    # projector matrix
    PP = np.zeros(3*N)
    PP[N:2*N] = 1
    P = np.diag(PP)

    # Finding Maximum growth
    U, S, Vt = la.svd(np.dot(P, A))

    #x = eigenvector of largest eigenvalue
    y = Vt[0,:]
    G = S[0]**2 #bTb < largest eigenvalue

    return G, y


def growth3(G,params=(2,2.8,1,1.0,0.5),T=6):
    """
    Question 2.3
    Find maximum possible growth, G=sum(Si Vi)/e(t=0)
    Input:
    G: Networkx graph
    params: contains model parameters, see code below.
    T: time for energy growth

    Output:
    G: Maximum growth

    Discussion:
    This problem varies slightly from growth1 and growth2, such that there does not exist a projection matrix that can multiply Si and Vi to give the maximum possible growth, G = sum(Si Vi)/e(t=0). In order to maximize SiVi, we can maximize both Si and Vi, using a projection matrix, P, to isolate Si and Vi, just like we did for growth2. When Si and Vi are simultaneously maximised, we can obtain the maximum growth of sum(SiVi). We take the SVD of Ap = np.dot(P, A), so this is equivalent to maximizing G' = sum(Si^2 + Vi^2)/e(t=0). We then find the respective values for S and V at time T and find the respective maximum possible growth G.
    """

    a,theta,g,k,tau=params
    N = G.number_of_nodes()

    #Construct flux matrix (use/modify as needed)
    Q = [t[1] for t in G.degree()]

    Pden = np.zeros(N)
    Pden_total = np.zeros(N)
    for j in G.nodes():
        for m in G.adj[j].keys():
            Pden[j] += Q[m]
    Pden = 1/Pden
    Q = tau*np.array(Q)
    F = nx.adjacency_matrix(G).toarray()
    F = F*np.outer(Q,Pden)
    #-------------------------------------

    #Add code here
    # Building matrix A for dydt = exp(A*t)*y
    zv = np.ones(N)
    colF = F.sum(axis=0)
    main = np.hstack([-(g+k)*zv-colF, -(k+a)*zv-colF, k*zv-colF])
    ad = np.hstack([a*zv, np.zeros(N)])
    td = np.hstack([theta*zv, np.zeros(N)])
    bd = -theta*zv
    M = np.diag(main) + np.diag(bd, -2*N) + np.diag(td, -N) + np.diag(ad, N) + la.block_diag(*([F] *3))

    #y(T) = exp(M*T)*y(0)
    A = la.expm(np.array(M)*T)

    # projector matrix
    PP = np.ones(3*N)
    PP[N:2*N] = 0
    P = np.diag(PP)

    # Finding Maximum growth
    Ap = np.dot(P, A)
    U, S, Vt = la.svd(Ap)
    y = Vt[0,:]
    yT = np.dot(Ap, y)
    G = sum((yT[:N]*yT[2*N:]))

    return G


def Inew(D):
    """
    Question 2.4

    Input:
    D: N x M array, each column contains I for an N-node network

    Output:
    I: N-element array, approximation to D containing "large-variance"
    behavior

    Discussion:
    We consider D as the data matrix, so then the covariance matrix, C is
    defined as DDT. The diagonal elements consist of the variances of each
    node, so the trace gives the "total variance" of D.

    We can first extract the "important" variables by projecting the data onto
    new variables such that the covariance matrix, C, is diagonalized. The
    method assumes that the columns of D have zero mean. However, since the
    columns of D do not necessarily sum to zero, we subtract the column mean
    from each column to form a new matrix, Dnew. With zero covariance, we can
    now consider the new variables to be independent. As such, the variable
    with a larger variance is more "important. This can be carried out by
    finding a projection matrix, F, such that we have G = FA, GGT = FAATFT = FCFT gives a diagonal matrix. Since D has zero mean, so does G. Now, we consider Dnew = USVt, so by performing SVD on Dnew, we can get U, S and Vt.  We note that FCFT = FUSUTFT = diagonal matrix, so we take F = U.T.

    By transforming the data Dnew with UT, such that G = UTDnew, the importance of the transformed data is given by eigenvalues stored in the vector S (squared of the elements in S). Since F = U.T is orthogonal, G and D have the same variance, so the trace of the covariance matrix of GGT and AAT are the same. Now the trace of the covariance matrix gives the "large variance" behaviour of the matrix, D. This would approximate the total variance of D.
    """
    N,M = D.shape

    #Add code here
    Dnew = D - D.mean(axis=0)
    U, S, Vt = la.svd(Dnew)
    # Transformed data G = UTD
    G = np.dot(U.T, Dnew)
    I = np.diagonal(np.dot(G, G.T))

    return I


if __name__=='__main__':
    #add/modify code here if/as desired
    N,M = 100,5
    G = nx.barabasi_albert_graph(N,M,seed=1)
    g1, y1 = growth1(G)
    g2, y2 = growth2(G)
    g3 = growth3(G)

    D = np.loadtxt('q22test.txt')
    I = Inew(D)
