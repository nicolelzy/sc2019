"""M345SC Homework 3, part 1
Nicole Zhi Ying LEE (01196496)
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
from scipy.signal import hann
from scipy.sparse import diags
import scipy.sparse.linalg as la
import time

def nwave(alpha,beta,Nx=256,Nt=801,T=200,display=False):
    """
    Question 1.1
    Simulate nonlinear wave model

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    Display: Function creates contour plot of |g| when true

    Output:
    g: Complex Nt x Nx array containing solution
    """

    #generate grid
    L = 100
    x = np.linspace(0,L,Nx+1)
    x = x[:-1]

    def RHS(f,t,alpha,beta):
        """Computes dg/dt for model eqn.,
        f[:N] = Real(g), f[N:] = Imag(g)
        Called by odeint below
        """
        g = f[:Nx]+1j*f[Nx:]
        #add code here
        n = np.fft.fftshift(np.arange(-Nx/2, Nx/2))
        k = 2*np.pi*n/L
        d2g = Nx*np.fft.ifft(-(k**2)*np.fft.fft(g)/Nx)
        #-----------
        dgdt = alpha*d2g + g - beta*g*g*g.conj()
        df = np.zeros(2*Nx)
        df[:Nx] = dgdt.real
        df[Nx:] = dgdt.imag
        return df

    #set initial condition
    g0 = np.random.rand(Nx)*0.1*hann(Nx)
    f0=np.zeros(2*Nx)
    f0[:Nx]=g0
    t = np.linspace(0,T,Nt)

    #compute solution
    f = odeint(RHS,f0,t,args=(alpha,beta))
    g = f[:,:Nx] + 1j*f[:,Nx:]

    if display:
        plt.figure()
        plt.contour(x,t,g.real)
        plt.xlabel('x')
        plt.ylabel('t')
        plt.title('Contours of Real(g)')

    return g

def analyze(input = (1-2j, 1+2j, 1-1j), Nx = 256, Nt = 801, T = 200):
    """
    Question 1.2
    Input:
    params = Contains α and β for case A and B, α1=1−2i, β=1+2i, α2=1−i
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]

    Output:
    gA: Complex Nt x Nx array containing solution for Case A
    gA: Complex Nt x Nx array containing solution for Case A
    r: Computed values of r = Gn+1/(Gn*(1-Gn))

    Discussion:
    Given the following two cases, A: α=1−2i, β=1+2i and B: α=1−i, β=1+2i, I
    removed the initial transient period of t<= 50.In the name==main portion of
    p1.py, I called analyze while varying Nx, Nt or T at one time. Then I plot
    the contour plots of the two cases for each simulation and use the values
    of r of the log map to determine the most energetic frequencies and to what
    degree they are chaotic.

    We first note that for each variant, the contour plot of case B is always
    denser than that of case A, so the contours are narrower and longer for B
    than A. Comparing figures 1, 4, 7, 10, 13, 16 and 19 with figures 2, 5, 8,
    11, 14, 17 and 20, we see that changing α=1−2i to α=1−i affects how the
    values are related with respect to x. This happens because α is the
    coefficient of ∂2g/∂x2.
    Fig1-9
    As Nx increases, the contour plots look the same across all Nx. However,
    the plot of r against time, t, (log map) has more defined and taller peaks
    as Nx increases, indicating that each case becomes increasingly chaotic as
    Nx increases. For small Nx (Nx = 128), we see that case B is less chaotic
    as there are shorter peaks and fewer outliers as compared to case A when Nx
    = 128. For Nx > 128, Case B becomes increasingly chaotic, with more
    outliers of the log map and more defined peaks. It can be seen that for
    approximately r < 4, this is weakly chaotic, but for large r, the cases are
    chaotic. The peaks correspond to the most energetic frequencies of each non-
    linear wave simulated.

    Fig4-6 and fig10-15
    Similarly, for Nt = 401, 801 and 1601, there are no significant differences
    of the contour plot within each case. However, the contours are slightly
    narrower as Nt increases, due to a finer differentation, and a smaller
    timestep, which increases the accuracy of the result. This is the case for
    both A and B. However, for large Nt, the wave is chaotic for both cases,
    with a greater degree for case B. So for large Nt, the results are less
    stable. For small Nt, the range of r values is rather small but there are
    outliers for case A, A is more chaotic for smaller Nt values, and B is more
    chaotic for larger Nt values.

    Fig4-6 and fig16-21
    Change in T from 100 to 200 and 400 affects the contour plots the most but the changes do not affect the degree of chaos of the waves. As T increases, the contour plots become increasingly dense, so only patches appear. For small T (T=100), we can clearly see that there are few peaks which correspond to the most energetic frequencies. Case A is less chaotic than case B. As T increases to 400, the r value fluctuates for rapidly, making the cases very chaotic. Both case A and B are very chaotic for large T.

    For the default case Nx = 256, Nt = 801 and T = 200, there is clearly a most energetic wave for Case A and B, where the largest peak occurs. The simulations are mostly non-chaotic as the range of r is small.
    """
    a1, beta, a2 = input

    skip = int(50/T*(Nt-1))
    r = np.zeros((Nt,2))
    t = np.linspace(0,T,Nt)
    x = np.linspace(0, 100, Nx+1)
    x = x[:-1]
    h = x[1] - x[0]
    gA = nwave(a1, beta, Nx, Nt, T)
    gB = nwave(a2, beta, Nx, Nt, T)

    plt.figure()
    plt.contour(x, t[skip:], gA[skip:,:].real)
    plt.xlabel('x')
    plt.ylabel('t')
    plt.title('Contours of Real(g) for Case A, Nx=%d, Nt=%d,T=%d'%(Nx, Nt, T))
    plt.colorbar()
    plt.show()

    plt.figure()
    plt.contour(x,t[skip:],gB[skip:,:].real)
    plt.xlabel('x')
    plt.ylabel('t')
    plt.title('Contours of Real(g) for Case A, Nx=%d, Nt=%d,T=%d'%(Nx, Nt, T))
    plt.colorbar()
    plt.show()

    for i in range(Nt-1):
        r[i,0] = gA[i+1, 100]/(gA[i,100]*(1-gA[i,100]))
        r[i,1] = gB[i+1, 100]/(gB[i,100]*(1-gB[i,100]))
    plt.figure()
    plt.plot(t[skip:], r[skip:,0], '-', label='Case A')
    plt.plot(t[skip:], r[skip:,1], '-', label='Case B')
    plt.xlabel('time, t')
    plt.ylabel('r')
    plt.legend()
    plt.title('Plot of r against time for Case A and B, Nx = %d, Nt = %d, T = %d'%(Nx, Nt, T))
    plt.show()

    return gA, gB, r


def wavediff(input=(1-1j, 1+2j), display=False):
    """
    Question 1.3

    Input:
    alpha, beta: complex model parameters
    Nx: Number of grid points in x
    Nt: Number of time steps
    T: Timespan for simulation is [0,T]
    input: contains tuple of alpha, beta,
           Nx, Nt, T used to generate g
    Display: Function plots difference between the two methods when true

    Output:
    meantime: array of mean time taken for varying Nx, Nt and T respectively

    Discussion:
    We use the simulation results of case B from analyze. From the figures
    generated, we can see that the finite difference (fd) method is not
    superior to the Fourier differentiation method for non-linear waves
    generated by the model for most of points within the period. Generally for
    5 < x < 95 (for L = 100), we have a zero difference between the two
    methods, for varying Nx, with Nt and T kept constant. We see that the end
    points vary the most. As Nx increases, the difference between the two
    methods increase in magnitude, from approximately 0.1 to 0.8. The
    difference between the two methods depend on the grid point number, so as
    Nx increases for a fixed input, the varied results (difference != 0) occurs
    for a smaller range of x. This is shown by fig22, fig23 and fig24.

    When Nt and T are varied, I noted that there were no significant
    differences between calculations, as in fig25 and fig26. On the whole, the
    ratio of computational time taken for the fd method to Fourier
    differentiation was noted. Since the ratio is always above 1, this
    indicates that the FD method is much slower than the FFT method. Hence,
    although the FD method has lower operation counts and require a lower
    memory usage than FFT, it is not a superior method. Hence, I conclude that
    FFT is superior to the FD method, since the latter is a local
    approximation, which could lead to errors from the exact value. In
    addition, the FD method is rather complicated and requires special care
    for the boundary conditions.

    Generally, for the reasons listed above, FFT is superior to FD and
    supported by the figures generated, and the result of the function
    meantime, which shows the mean time for varying Nx, Nt and T. While for
    simpler functions, FD would be superior, in the event of complex
    geometries, the Fourier method is superior and cost less to compute,
    particularly for the case at hand.
    """
     # Putting calculations in a function to call repeatedly
    def diffc(input, Nx, Nt, T):
        # Specifying input values
        ap, bt = input
        ts = int(100/T*(Nt-1))
        L = 100
        x = np.linspace(0,L,Nx+1)
        x = x[:-1]
        h = x[1]-x[0]

        # Discrete FT
        g = nwave(ap, bt, Nx, Nt, T)
        t1 = time.time()
        n = np.fft.fftshift(np.arange(-Nx/2, Nx/2))
        k = 2*np.pi*n/L
        dgFT = Nx*np.fft.ifft(1j*k*np.fft.fft(g[ts,:])/Nx)
        t2 = time.time()
        dtFT = t2-t1

        # Building tridiagonal matrix
        alpha = 3/8
        a, b, c = 25/16, 1/5, -1/80
        zv = np.ones(Nx)
        av = alpha*zv[1:]
        av[0] = 3
        A = diags([av[::-1],zv,av],[-1,0,1])

        # Building matrix to compute RHS
        main = np.zeros(Nx)
        main[0], main[-1] = -17/3, 17/3 #a*2 because the matrix is multiplied by 1/2h after
        ov = np.ones(Nx)
        ad, bd, cd = a*ov[1:], (b/2)*ov[2:], (c/3)*ov[3:]
        ad[0], bd[0], cd[0] = 3, 3, -1/3 #b*2, c*2 because the matrix is multiplied by 1/2h after
        bd[1], cd[1], cd[2] = 0, 0, 0
        main, ad, bd, cd = main/(2*h), ad/(2*h), bd/(2*h), cd/(2*h)
        M = diags([-cd[::-1], -bd[::-1], -ad[::-1], main, ad, bd, cd], [-3, -2, -1, 0, 1, 2, 3])

        # Computing RHS
        b = np.dot(M.toarray(), g[ts,:])

        t1 = time.time()
        # Solving for Finite Difference
        dgFD = la.spsolve(A, b)
        t2 = time.time()
        dtFD = t2-t1

        return x, dtFD, dtFT, dgFD, dgFT

    # Specifying input, determining timestep and h
    Nxr = [128, 512, 256]
    Ntr = [201, 801]
    tr = [100, 400]

    timeNx, timeNt, timeT = np.zeros(3), np.zeros(3), np.zeros(3)

    # Loop through different Nx
    for i, Nx in enumerate(Nxr):
        x, dtFD, dtFT, dgFD, dgFT = diffc(input, Nx, 401, 200)
        timeNx[i] = dtFD/dtFT # Tracking Time

        # Plotting difference
        diff = dgFD-dgFT

        if display:
            plt.figure()
            plt.plot(x, diff.real)
            plt.xlabel('x')
            plt.ylabel('Difference')
            plt.title('Difference between Finite Difference Method and Fourier Transform Method for Nx = %d at time t = 100, (Nt=801, T=200)'%Nx)
            plt.show()

    diffN, diffT = np.zeros((256,3)), np.zeros((256,3))
    diffN[:,0], diffT[:,0] = diff, diff
    timeNt[0], timeT[0] = timeNx[2], timeNx[2]

    # Loop through different Nt
    for i, Nt in enumerate(Ntr):
        x, dtFD, dtFT, dgFD, dgFT = diffc(input, 256, Nt, 200)
        timeNt[i+1] = dtFD/dtFT # Tracking Time

        # Plotting difference
        diffN[:,i+1] = dgFD-dgFT

    if display:
        Ntr = [401] + Ntr
        plt.figure()
        for i, n in enumerate(Ntr):
            plt.plot(x, diffN[:,i].real, label='Nt =%d'%n)
        plt.xlabel('x')
        plt.ylabel('Difference')
        plt.legend()
        plt.title('Difference between Finite Difference Method and Fourier Transform Method for different at time t = 100, (Nx=512, T=200)')
        plt.show()

    # Loop through different T
    for i, t in enumerate(tr):
        x, dtFD, dtFT, dgFD, dgFT = diffc(input, 256, 401, t)
        timeT[i+1] = dtFD/dtFT # Tracking Time

        # Plotting difference
        diffT[:,i+1] = dgFD-dgFT

    if display:
        tr = [200] + tr
        plt.figure()
        for i, t in enumerate(tr):
            plt.plot(x, diffT[:,i].real, label = 'T = %d'%t)
        plt.xlabel('x')
        plt.ylabel('Difference')
        plt.legend()
        plt.title('Difference between Finite Difference Method and Fourier Transform Method for T = %d at time t = 100, (Nx=512, Nt=801)'%t)
        plt.show()

    meantime = [timeNx.mean(), timeNt.mean(), timeT.mean()]
    return meantime

if __name__=='__main__':
    #Add code here to call functions above and
    #generate figures you are submitting
    g = nwave(1-1j, 1+2j)
    gNx128_A, gNx128_B, rNx128 = analyze(Nx = 128)
    gNx256_A, gNx256_B, rNx256 = analyze()
    gNx512_A, gNx512_B, rNx512 = analyze(Nx = 512)
    gNt401_A, gNt401_B, rNt401 = analyze(Nt=401)
    gNt1601_A, gNt1601_B, rNt1601 = analyze(Nt=1601)
    gT100_A, gT100_B, rT100 = analyze(T=100)
    gT400_A, gT400_B, rT400 = analyze(T = 400)
    meantime = wavediff(display=True)
